<?php
use JohnnyCash\Johnny;

$conn = OpenCon(); // db connection

// getting inputs for time interval in which we want to determine top selling product
if($_GET){
	$from = $_GET['time_from'];
	$to = $_GET['time_to'];
}else{
	$from = "2019-09-01 00:00:00";
	$to = "2019-09-30 23:59:59";
}


$orderLog = new Johnny($conn); 
$orderLog->findTopSellingProducts($from,$to); // (from_date, to_date)

// sending output in json
echo(json_encode($orderLog->getTopSellingproducts()));

CloseCon($conn);
