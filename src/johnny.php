<?php
Namespace JohnnyCash;

Class Johnny{

	private $topSellingProducts = [];
	private $sql;

	public function __construct($conn)
	{
		$this->sql = $conn;
	}


/***
** Finds top selling products based on from_date string and to_date string. more than one products can be top sellers
**/
	public function findTopSellingProducts($time_from = '2019-09-01 00:00:00', $time_to = '2019-09-30 23:59:59'): void
	{
		$query = "SELECT skuId, name, sum(quantity) as 'Units Sold' FROM `johnnyorderlog` INNER JOIN `johnnysku` ON `johnnyorderlog`.`skuId` = `johnnysku`.`id`
				where time_created >= '".$time_from."' AND time_created <= '".$time_to."' GROUP BY skuId having sum(quantity) = (SELECT sum(quantity) 
				FROM `johnnyorderlog` where time_created >= '".$time_from."' AND time_created <= '".$time_to."' GROUP BY skuId ORDER BY sum(quantity) DESC LIMIT 0,1)";

		$this->query_runner($query);
	}


/***
** Gets top selling products
**/
	public function getTopSellingproducts(): array
	{		
		return $this->topSellingProducts;
	}


/***
** Runs queries and sets private data
**/
	public function query_runner($q): void
	{
		$result = $this->sql->query($q);
		if ($result->num_rows > 0) {
			while($row = $result->fetch_assoc()) {
				$this->topSellingProducts[] = $row;
			}
		}
	}

}