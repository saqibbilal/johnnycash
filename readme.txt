I used composer and PHP 7.3.12.

I am auto-loading classes from src directory using composer with namespace: JohnnyCash and using psr-4 standard

Please follow these steps to setup:
    paste/extract "JohnnyCash" directory in a php environment and composer should be included at the path 
    or
    in xampp/htdocs directory if you use xampp 
    or 
    www directory if you use wamp

run the following command to generate "vendor" directory:
    composer install

------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------

q1. 
Through Code: Go to johnny_cash.php file and provide "from_date" and "to_date" respectively. save the file and run index.php file

Through Postman: make a GET request to johnnyCash directory and set parameters for:
	time_from
	time_to
(example format: 2019-09-30 23:59:59)

------------------------------------------------------------------------------------------------------

q2. SQL constraints have been added. I added another column in "johnnysku" for keeping track of stock.

------------------------------------------------------------------------------------------------------
q3.
we shoud have another column "in_stock" in johnnysku table to store available stock of the product. The quantity in this column should be managed with respect to new orders in johnnyorderlog. this can be done either server side script or SQL triggers.
we should have a cron job set that runs on on a certain time, for simplicity lets say every evening or every morning and triggers a server side script.

In this script we can check the johnnysku table for products that have "im_stock" value less than 20 or any threshold we want. we prepare a report from johnnysku using id, product name and in_store columns and email it to respective department that manages stock state.

in the same script, we can add a block that checks johnnyorderlog for SUM(totalPrice) - Sum(paidInBox). if the answer is greater than a set amount e.g. $500 for an employee, then a notification should be triggered to that employee to clear his payments.